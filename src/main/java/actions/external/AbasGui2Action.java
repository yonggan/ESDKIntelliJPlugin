package actions.external;

import actions.settings.ESDKPluginSettingsPersistence;
import com.intellij.openapi.actionSystem.AnAction;
import com.intellij.openapi.actionSystem.AnActionEvent;
import org.jetbrains.annotations.NotNull;
import utils.Notifications;

import java.io.File;
import java.io.IOException;

/**
 * The type Abas gui 2 action.
 */
public class AbasGui2Action extends AnAction {
    @Override
    public void actionPerformed(@NotNull final AnActionEvent e) {
        final ESDKPluginSettingsPersistence instance = ESDKPluginSettingsPersistence.getInstance();

        if (instance != null && instance.getAbasGui2Path() != null && !instance.getAbasGui2Path().isEmpty()) {
            try {
                final ProcessBuilder processBuilder = new ProcessBuilder(instance.getAbasGui2Path());
                processBuilder.directory(new File(instance.getAbasGui2Path()).getParentFile());
                processBuilder.start();
            } catch (final IOException ex) {
                Notifications.errorNotification("Could not start Abas GUI 2! Please check Path in Settings!");
                System.out.println(ex.getMessage());
            }
        }
    }
}
