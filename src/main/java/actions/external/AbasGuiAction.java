package actions.external;

import actions.settings.ESDKPluginSettingsPersistence;
import com.intellij.openapi.actionSystem.AnAction;
import com.intellij.openapi.actionSystem.AnActionEvent;
import org.jetbrains.annotations.NotNull;
import utils.Notifications;

import java.io.File;
import java.io.IOException;

/**
 * The type Abas gui action.
 */
public class AbasGuiAction extends AnAction {
    @Override
    public void actionPerformed(@NotNull final AnActionEvent e) {
        final ESDKPluginSettingsPersistence instance = ESDKPluginSettingsPersistence.getInstance();
        if (instance != null && instance.getAbasGuiPath() != null && !instance.getAbasGuiPath().isEmpty()) {
            try {
                final ProcessBuilder processBuilder = new ProcessBuilder(instance.getAbasGuiPath());
                processBuilder.directory(new File(instance.getAbasGuiPath()).getParentFile());
                processBuilder.start();
            } catch (final IOException ex) {
                Notifications.errorNotification("Could not start Abas GUI! Please check Path in Settings!");
                System.out.println(ex.getMessage());
            }
        }
    }
}
