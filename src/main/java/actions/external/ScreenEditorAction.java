package actions.external;

import actions.settings.ESDKPluginSettingsPersistence;
import com.intellij.openapi.actionSystem.AnAction;
import com.intellij.openapi.actionSystem.AnActionEvent;
import org.jetbrains.annotations.NotNull;
import utils.Notifications;

import java.io.IOException;

/**
 * The type Screen editor action.
 */
public class ScreenEditorAction extends AnAction {
    @Override
    public void actionPerformed(@NotNull final AnActionEvent e) {
        final ESDKPluginSettingsPersistence instance = ESDKPluginSettingsPersistence.getInstance();
        if (instance != null && !instance.getScreenEditorPath().isEmpty()) {
            try {
                Runtime.getRuntime().exec(instance.getScreenEditorPath());
            } catch (final IOException ex) {
                Notifications.errorNotification("Could not start ScreenEditor! Please check Path in Settings!");
                System.out.println(ex.getMessage());
            }
        }
    }
}
