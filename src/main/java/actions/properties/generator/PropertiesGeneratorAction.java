package actions.properties.generator;

import com.intellij.openapi.actionSystem.AnAction;
import com.intellij.openapi.actionSystem.AnActionEvent;

public class PropertiesGeneratorAction extends AnAction {
    @Override
    public void actionPerformed(final AnActionEvent e) {
        PropertiesGenerator.main(e.getProject());
    }
}
