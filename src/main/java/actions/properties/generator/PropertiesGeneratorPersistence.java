package actions.properties.generator;

import com.intellij.openapi.components.PersistentStateComponent;
import com.intellij.openapi.components.ServiceManager;
import com.intellij.openapi.components.State;
import com.intellij.openapi.components.Storage;
import com.intellij.util.xmlb.XmlSerializerUtil;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.HashMap;
import java.util.Map;

/**
 * Saving ESDK Plugin Settings persistent.
 */
@State(
        name = "ESDKPPropertiesGeneratorPersistence",
        storages = {
                @Storage(value = "EDKPropertiesGenerator.xml")}
)
public class PropertiesGeneratorPersistence implements PersistentStateComponent<PropertiesGeneratorPersistence> {

    private Map<String, String> properties;

    /**
     * Instantiates a new Properties manager persistence.
     */
    public PropertiesGeneratorPersistence() {
    }

    @Nullable
    public static PropertiesGeneratorPersistence getInstance() {
        return ServiceManager.getService(PropertiesGeneratorPersistence.class);
    }

    @Nullable
    @Override
    public PropertiesGeneratorPersistence getState() {
        return this;
    }

    @Override
    public void loadState(@NotNull final PropertiesGeneratorPersistence propertiesManagerPersistence) {
        XmlSerializerUtil.copyBean(propertiesManagerPersistence, this);
    }

    public Map<String, String> getProperties() {
        if (properties == null) this.properties = new HashMap<>();
        return properties;
    }

    public void setProperties(final Map<String, String> properties) {
        this.properties = properties;
    }
}
