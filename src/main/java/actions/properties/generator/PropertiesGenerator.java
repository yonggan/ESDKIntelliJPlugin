package actions.properties.generator;

import com.intellij.openapi.project.Project;
import com.intellij.openapi.roots.ProjectRootManager;
import com.intellij.openapi.vfs.VirtualFile;
import com.intellij.ui.components.JBList;
import utils.FileUtils;
import utils.Notifications;

import javax.swing.*;
import java.awt.event.KeyEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.File;
import java.io.IOException;
import java.util.Map;

public class PropertiesGenerator extends JDialog {
    private static Project project;
    private static DefaultListModel<String> defaultListModel = new DefaultListModel<String>();
    private JPanel contentPane;
    private JButton buttonOK;
    private JList<String> propertiesGeneratorList;
    private JTextArea propertiesText;
    private JButton addProperties;
    private JButton generateProperties;
    private JButton deleteProperties;
    private JTextField propertiesName;

    public PropertiesGenerator() {
        setContentPane(contentPane);
        setModal(true);
        getRootPane().setDefaultButton(buttonOK);

        buttonOK.addActionListener(e -> onOK());

        generateProperties.addActionListener(e -> generateProperties());
        addProperties.addActionListener(e -> addProperties());
        deleteProperties.addActionListener(e -> deleteProperties());

        // call onCancel() when cross is clicked
        setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
        addWindowListener(new WindowAdapter() {
            public void windowClosing(final WindowEvent e) {
                dispose();
            }
        });

        // call onCancel() on ESCAPE
        contentPane.registerKeyboardAction(e -> dispose(), KeyStroke.getKeyStroke(KeyEvent.VK_ESCAPE, 0),
                JComponent.WHEN_ANCESTOR_OF_FOCUSED_COMPONENT);
    }

    public static void main(final Project pProject) {
        project = pProject;
        final PropertiesGenerator dialog = new PropertiesGenerator();
        dialog.pack();
        dialog.setVisible(true);
    }

    private void deleteProperties() {
        if (!propertiesGeneratorList.isSelectionEmpty()) {
            final PropertiesGeneratorPersistence instance = PropertiesGeneratorPersistence.getInstance();
            if (instance != null) {
                instance.getProperties()
                        .remove(propertiesGeneratorList.getSelectedValue());
                loadProperties();
            }
        }
    }

    private void addProperties() {
        if (propertiesName.getText().isEmpty() || propertiesText.getText().isEmpty()) return;
        final PropertiesGeneratorPersistence instance = PropertiesGeneratorPersistence.getInstance();
        if (instance != null) {
            instance.getProperties().put(propertiesName.getText(), propertiesText.getText());
            loadProperties();
        }
    }

    private void generateProperties() {
        if (!propertiesGeneratorList.isSelectionEmpty()) {
            final PropertiesGeneratorPersistence instance = PropertiesGeneratorPersistence.getInstance();
            if (instance != null) {
                final Map<String, String> properties = instance.getProperties();
                final String path = project.getBasePath() + "/gradle.properties." + propertiesGeneratorList.getSelectedValue();
                if (new File(path).exists()) {
                    Notifications.errorNotification("Gradle Properties with this name already exists!");
                    return;
                }
                try {
                    new File(path).createNewFile();
                    FileUtils.writeAllToFile(path, properties.get(propertiesGeneratorList.getSelectedValue()));
                    for (final VirtualFile virtualFile : ProjectRootManager.getInstance(project).getContentRootsFromAllModules()) {
                        virtualFile.refresh(true, true);
                    }
                } catch (final IOException e) {
                    e.printStackTrace();
                }
                dispose();
            }
        }
    }

    private void onOK() {
        // add your code here
        dispose();
    }

    private void createUIComponents() {
        // TODO: place custom component creation code here
        propertiesGeneratorList = new JBList<>(defaultListModel);
        loadProperties();
    }

    private void loadProperties() {
        final PropertiesGeneratorPersistence instance = PropertiesGeneratorPersistence.getInstance();
        if (instance != null) {
            defaultListModel.clear();
            for (final Map.Entry<String, String> entry : instance.getProperties().entrySet()) {
                defaultListModel.addElement(entry.getKey());
            }
            propertiesGeneratorList.updateUI();
        }
    }
}
