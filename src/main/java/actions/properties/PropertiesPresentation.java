package actions.properties;

import com.intellij.openapi.project.Project;
import com.intellij.openapi.roots.ProjectRootManager;
import com.intellij.openapi.ui.popup.*;
import com.intellij.openapi.vfs.VirtualFile;
import com.intellij.openapi.wm.StatusBarWidget;
import com.intellij.util.Consumer;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import utils.FileUtils;

import javax.swing.*;
import java.awt.event.MouseEvent;
import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * The type Properties presentation.
 */
public class PropertiesPresentation implements StatusBarWidget.MultipleTextValuesPresentation {

    private Project project;
    private ArrayList<File> gradleProperties;

    /**
     * Instantiates a new Properties presentation.
     *
     * @param project the project
     */
    public PropertiesPresentation(final Project project) {
        this.project = project;
        updatePropertiesManagerPersistence();
    }

    @Nullable("null means the widget is unable to show the popup")
    @Override
    public ListPopup getPopupStep() {
        final JBPopupFactory jbPopupFactory = JBPopupFactory.getInstance();
        updatePropertiesManagerPersistence();
        return jbPopupFactory.createListPopup(
                new ListPopupStep<File>() {
                    @Nullable
                    @Override
                    public String getTitle() {
                        return "Gradle Properties";
                    }

                    @Nullable
                    @Override
                    public PopupStep onChosen(final File selectedValue, final boolean finalChoice) {
                        final File[] currentFiles = FileUtils.getAllFilesInFirstLevelDirectoryWithFilter(project.getBasePath(), "gradle\\.properties");
                        final PropertiesManagerPersistence instance = PropertiesManagerPersistence.getInstance(project);
                        if (instance != null) {
                            if (instance.getOldProperties() == null) {
                                if (currentFiles.length > 0) {
                                    currentFiles[0].renameTo(new File(project.getBasePath() + "/gradle.properties.original"));

                                }
                            } else {
                                new File(project.getBasePath() + "/gradle.properties").renameTo(new File(instance.getOldProperties()));
                            }
                            instance.setOldProperties(selectedValue.getAbsolutePath());
                            selectedValue.renameTo(new File(project.getBasePath() + "/gradle.properties"));
                            for (final VirtualFile virtualFile : ProjectRootManager.getInstance(project).getContentRootsFromAllModules()) {
                                virtualFile.refresh(true, true);
                            }
                        }
                        return null;
                    }

                    @Override
                    public boolean hasSubstep(final File selectedValue) {
                        return false;
                    }

                    @Override
                    public void canceled() {

                    }

                    @Override
                    public boolean isMnemonicsNavigationEnabled() {
                        return false;
                    }

                    @Nullable
                    @Override
                    public MnemonicNavigationFilter<File> getMnemonicNavigationFilter() {
                        return null;
                    }

                    @Override
                    public boolean isSpeedSearchEnabled() {
                        return false;
                    }

                    @Nullable
                    @Override
                    public SpeedSearchFilter<File> getSpeedSearchFilter() {
                        return null;
                    }

                    @Override
                    public boolean isAutoSelectionEnabled() {
                        return false;
                    }

                    @Nullable
                    @Override
                    public Runnable getFinalRunnable() {
                        return null;
                    }

                    @NotNull
                    @Override
                    public List<File> getValues() {
                        return gradleProperties;
                    }

                    @Override
                    public boolean isSelectable(final File value) {
                        return true;
                    }

                    @Nullable
                    @Override
                    public Icon getIconFor(final File aValue) {
                        return null;
                    }

                    @NotNull
                    @Override
                    public String getTextFor(final File value) {
                        return value.getName().replaceAll("gradle\\.properties\\.", "");
                    }

                    @Nullable
                    @Override
                    public ListSeparator getSeparatorAbove(final File value) {
                        return null;
                    }

                    @Override
                    public int getDefaultOptionIndex() {
                        return 0;
                    }
                });
    }

    @Nullable
    @Override
    public String getSelectedValue() {
        final PropertiesManagerPersistence propertiesManagerPersistence = PropertiesManagerPersistence.getInstance(project);
        if (propertiesManagerPersistence != null && propertiesManagerPersistence.getOldProperties() != null) {
            System.out.println(new File(propertiesManagerPersistence.getOldProperties()).getName().replaceAll("gradle\\.properties\\.", ""));
            return new File(propertiesManagerPersistence.getOldProperties()).getName().replaceAll("gradle\\.properties\\.", "");
        } else {
            return "original";
        }
    }

    @NotNull
    @Override
    public String getMaxValue() {
        return "Max Value";
    }


    @Nullable
    @Override
    public String getTooltipText() {
        return "Gradle Properties Manager";
    }

    @Nullable
    @Override
    public Consumer<MouseEvent> getClickConsumer() {
        return null;
    }

    private void updatePropertiesManagerPersistence() {
        gradleProperties = new ArrayList<File>(Arrays.asList(FileUtils.getAllFilesInFirstLevelDirectoryWithFilter(project.getBasePath(), "gradle\\.properties.+")));
    }
}

