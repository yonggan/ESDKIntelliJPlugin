package actions.properties;

import com.intellij.openapi.project.Project;
import com.intellij.openapi.wm.impl.status.EditorBasedWidget;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

/**
 * The type Properties widget.
 */
public class PropertiesWidget extends EditorBasedWidget {

    /**
     * Instantiates a new Properties widget.
     *
     * @param project the project
     */
    public PropertiesWidget(@NotNull final Project project) {
        super(project);
    }

    @NotNull
    @Override
    public String ID() {
        return "ESDKGradlePropertiesWidget";
    }

    @Nullable
    @Override
    public WidgetPresentation getPresentation(@NotNull final PlatformType type) {
        return new PropertiesPresentation(super.getProject());
    }
}
