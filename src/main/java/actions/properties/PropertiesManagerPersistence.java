package actions.properties;

import com.intellij.openapi.components.PersistentStateComponent;
import com.intellij.openapi.components.ServiceManager;
import com.intellij.openapi.components.State;
import com.intellij.openapi.components.Storage;
import com.intellij.openapi.project.Project;
import com.intellij.util.xmlb.XmlSerializerUtil;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

/**
 * Saving ESDK Plugin Settings persistent.
 */
@State(
        name = "ESDKPPropertiesManagerPersistence",
        storages = {
                @Storage(value = "ESDKPPropertiesManagerPersistence.xml")}
)
public class PropertiesManagerPersistence implements PersistentStateComponent<PropertiesManagerPersistence> {

    private String oldProperties;

    /**
     * Instantiates a new Properties manager persistence.
     */
    public PropertiesManagerPersistence() {
    }


    /**
     * Gets instance.
     *
     * @param project the project
     * @return the instance
     */
    @Nullable
    public static PropertiesManagerPersistence getInstance(final Project project) {
        if (project.isDisposed()) return null;
        return ServiceManager.getService(project, PropertiesManagerPersistence.class);
    }

    @Nullable
    @Override
    public PropertiesManagerPersistence getState() {
        return this;
    }

    @Override
    public void loadState(@NotNull final PropertiesManagerPersistence propertiesManagerPersistence) {
        XmlSerializerUtil.copyBean(propertiesManagerPersistence, this);
    }

    /**
     * Gets old properties.
     *
     * @return the old properties
     */
    public String getOldProperties() {
        return oldProperties;
    }

    /**
     * Sets old properties.
     *
     * @param oldProperties the old properties
     */
    public void setOldProperties(final String oldProperties) {
        this.oldProperties = oldProperties;
    }

}
