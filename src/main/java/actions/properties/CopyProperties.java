package actions.properties;

import com.intellij.openapi.command.WriteCommandAction;
import com.intellij.openapi.project.Project;
import com.intellij.openapi.vfs.VirtualFile;
import module.builder.ProjectReader;
import utils.Notifications;

import javax.swing.*;
import java.awt.event.KeyEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;

/**
 * The type Copy properties.
 */
public class CopyProperties extends JDialog {
    private static Project project;
    private static VirtualFile sourceFile;
    private JPanel contentPane;
    private JButton buttonOK;
    private JButton buttonCancel;
    private JTextField newClient;

    /**
     * Instantiates a new Copy properties.
     */
    public CopyProperties() {
        setContentPane(contentPane);
        setModal(true);
        getRootPane().setDefaultButton(buttonOK);

        buttonOK.addActionListener(e -> onOK());

        buttonCancel.addActionListener(e -> onCancel());

        // call onCancel() when cross is clicked
        setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
        addWindowListener(new WindowAdapter() {
            public void windowClosing(final WindowEvent e) {
                onCancel();
            }
        });

        // call onCancel() on ESCAPE
        contentPane.registerKeyboardAction(e -> onCancel(), KeyStroke.getKeyStroke(KeyEvent.VK_ESCAPE, 0), JComponent.WHEN_ANCESTOR_OF_FOCUSED_COMPONENT);
    }

    /**
     * Main.
     *
     * @param pProject    the p project
     * @param pSourceFile the p source file
     */
    public static void main(final Project pProject, final VirtualFile pSourceFile) {
        project = pProject;
        sourceFile = pSourceFile;
        final CopyProperties dialog = new CopyProperties();
        dialog.pack();
        dialog.setVisible(true);
    }

    private void onOK() {
        // add your code here
        if (!newClient.getText().isEmpty() && sourceFile.getName().contains("gradle.properties")) {
            WriteCommandAction.runWriteCommandAction(project, () -> {
                try {
                    final VirtualFile copy;
                    if (sourceFile.getName().equals("gradle.properties"))
                        copy = sourceFile.copy(this, sourceFile.getParent(),
                                sourceFile.getName() + "." + newClient.getText().trim());
                    else
                        copy = sourceFile.copy(this, sourceFile.getParent(),
                                sourceFile.getNameWithoutExtension() + "." + newClient.getText().trim());
                    String content = new String(Files.readAllBytes(Paths.get(copy.getPath())), StandardCharsets.UTF_8);
                    content = content.replaceAll(ProjectReader.getFirstMatcherGroup("ABAS_CLIENTID=(.*)", content).trim(),
                            newClient.getText().trim());
                    Files.write(Paths.get(copy.getPath()), content.getBytes(StandardCharsets.UTF_8));
                } catch (final IOException e) {
                    Notifications.errorNotification("Could not copy gradle.properties!");
                }
            });
            dispose();
        }
    }

    private void onCancel() {
        // add your code here if necessary
        dispose();
    }
}
