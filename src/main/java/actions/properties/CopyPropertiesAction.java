package actions.properties;

import com.intellij.openapi.actionSystem.AnAction;
import com.intellij.openapi.actionSystem.AnActionEvent;
import com.intellij.openapi.actionSystem.DataKeys;
import com.intellij.openapi.vfs.VirtualFile;

/**
 * The type Copy properties action.
 */
public class CopyPropertiesAction extends AnAction {
    @Override
    public void actionPerformed(final AnActionEvent e) {
        final VirtualFile currentFile = e.getData(DataKeys.VIRTUAL_FILE);
        if (currentFile.getName().contains("gradle.properties"))
            new CopyProperties().main(e.getProject(), currentFile);
    }
}
