package actions.eventRegistration;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.intellij.ide.util.PropertiesComponent;
import com.intellij.openapi.actionSystem.AnAction;
import com.intellij.openapi.actionSystem.AnActionEvent;
import com.intellij.openapi.application.ReadAction;
import com.intellij.openapi.command.WriteCommandAction;
import com.intellij.openapi.diagnostic.Logger;
import com.intellij.openapi.progress.ProgressIndicator;
import com.intellij.openapi.progress.ProgressManager;
import com.intellij.openapi.progress.Task;
import com.intellij.openapi.roots.OrderRootType;
import com.intellij.openapi.roots.ProjectRootManager;
import com.intellij.openapi.vfs.LocalFileSystem;
import com.intellij.openapi.vfs.VirtualFile;
import com.intellij.psi.PsiAnnotation;
import com.intellij.psi.PsiClass;
import com.intellij.psi.impl.search.JavaFilesSearchScope;
import com.intellij.psi.search.searches.AllClassesSearch;
import com.intellij.util.Query;
import module.builder.ProjectBuilder;
import module.builder.ProjectReader;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.java.decompiler.IdeaDecompiler;
import utils.FileUtils;
import utils.Notifications;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.Map;

/**
 * This class registeres all EventHandler in fop.json
 */
public class RegisterAllEventHandler extends AnAction {
    //
    private static final String LEGAL_NOTICE_KEY = "decompiler.legal.notice.accepted";

    @Override
    public void actionPerformed(@NotNull final AnActionEvent e) {
        ProgressManager.getInstance().run(new Task.Backgroundable(e.getProject(), "ESDK Event Registration", false) {
            @Override
            public void run(@NotNull final ProgressIndicator progressIndicator) {
                setJson(e, progressIndicator);
            }
        });
    }


    private void setJson(@NotNull final AnActionEvent e, @NotNull final ProgressIndicator progressIndicator) {
        progressIndicator.setFraction(0.0);
        final String fopJson = e.getProject().getBasePath() + "/src/main/resources/fop.json";
        final String tmpDirectory = "/tmp";


        if (!PropertiesComponent.getInstance().isValueSet(LEGAL_NOTICE_KEY)) {
            PropertiesComponent.getInstance().setValue(LEGAL_NOTICE_KEY, true);
        }
        progressIndicator.setFraction(0.1);
        final IdeaDecompiler decompiler = new IdeaDecompiler();
        final HashMap<String, String> psiClassStringHashMap = new HashMap<>();


        try {
            if (Files.exists(Paths.get(fopJson)))
                FileUtils.deleteFile(fopJson);
            if (Files.exists(Paths.get(e.getProject().getBasePath() + tmpDirectory)))
                FileUtils.deleteDirectoryStream(e.getProject().getBasePath() + tmpDirectory);
        } catch (@NotNull final IOException ignore) {
            Logger.getInstance(getClass()).warn(ignore);
        }
        progressIndicator.setFraction(0.2);
        ReadAction.run(() -> {

            final Query<PsiClass> search = AllClassesSearch.search(new JavaFilesSearchScope(e.getProject()), e.getProject());
            for (final PsiClass psiClass : search) {
                progressIndicator.setText("Searching Class: " + psiClass);
                for (final PsiAnnotation psiAnnotation : psiClass.getAnnotations()) {
                    psiAnnotation.getAttributes().get(0)
                            .getAttributeValue();
                    if (psiAnnotation.getQualifiedName().equals("de.abas.erp.axi2.annotation.EventHandler")) {
                        psiClassStringHashMap.put(psiClass.getQualifiedName(), psiAnnotation.findAttributeValue("head").getText());
                        break;
                    }
                }
            }
        });
        progressIndicator.setFraction(0.3);
        {
            for (final Map.Entry<String, String> entry : psiClassStringHashMap.entrySet()) {
                ReadAction.run(() -> {
                    ProjectRootManager.getInstance(e.getProject()).orderEntries().forEachLibrary(library -> {
                        for (final VirtualFile virtualFile : library.getFiles(OrderRootType.CLASSES)) {
                            try {
                                FileUtils.unpackClassInJarArchive(e.getProject().getBasePath() + tmpDirectory,
                                        virtualFile.getPath().replaceAll("!", ""), entry.getValue(), progressIndicator);
                            } catch (@NotNull final IOException ex) {
                                Logger.getInstance(getClass()).error(ex);
                                Notifications.errorNotification("Could not unpack Librarys!");
                            }
                        }
                        return true;
                    });
                });
                progressIndicator.setFraction(progressIndicator.getFraction() + 0.5 / psiClassStringHashMap.size());
            }
            progressIndicator.setFraction(0.8);
            for (final Map.Entry<String, String> entry : psiClassStringHashMap.entrySet()) {
                try {
                    Files.walk(Paths.get(e.getProject().getBasePath() + tmpDirectory))
                            .filter(Files::isRegularFile)
                            .forEach((f) -> {
                                JsonArray jsonArray = ProjectReader.readFopJson(e.getProject().getBasePath());
                                if (jsonArray == null) jsonArray = new JsonArray();
                                final String file = f.toString();
                                progressIndicator.setText(file);
                                if (file.endsWith(entry.getValue())) {
                                    if (LocalFileSystem.getInstance().findFileByPath(file) != null) {
                                        final String decompiledClass = decompiler.getText(LocalFileSystem.getInstance().findFileByPath(file)).toString();
                                        final String maskDatabaseName = ProjectReader.getFirstMatcherGroup("@DatabaseAnnotation\\([.*\\r*\\s*\\S*]*name\\s*=\\s*\\\"(.*)\\\"[.*\\r*\\s*\\S*]*\\@", decompiledClass);
                                        final JsonObject jsonObject = new JsonObject();

                                        if (!maskDatabaseName.isEmpty()) {
                                            //Database
                                            jsonObject.addProperty("databaseName", "(" + maskDatabaseName + ")");
                                            jsonObject.addProperty("groupName", "(" + entry.getValue().replace(".class", "").replace("Editor", "") + ")");

                                            jsonObject.addProperty("editorMode", "*");
                                            jsonObject.addProperty("event", "*");
                                            jsonObject.addProperty("key", "*");
                                            jsonObject.addProperty("field", "*");
                                            jsonObject.addProperty("headOrTable", "*");
                                            jsonObject.addProperty("isContinue", "[C]");
                                            jsonObject.addProperty("handler", "java:"
                                                    + entry.getKey()
                                                    + "@"
                                                    + ProjectReader.readAppID(e.getProject().getBasePath()));
                                            jsonArray.add(jsonObject);
                                            FileUtils.deleteFile(fopJson);
                                            final JsonArray finalJsonArray = jsonArray;
                                            WriteCommandAction.runWriteCommandAction(e.getProject(), () -> {
                                                ProjectBuilder.writeFopJson(e.getProject().getBasePath(), finalJsonArray);
                                            });
                                        }
                                    }
                                }
                            });
                    progressIndicator.setFraction(progressIndicator.getFraction() + 0.1 / psiClassStringHashMap.size());
                } catch (@NotNull final IOException ex) {
                    Logger.getInstance(getClass()).error(ex);
                    Notifications.errorNotification("Could not parse JSON!");
                }
            }
            progressIndicator.setFraction(1);
        }
    }
}