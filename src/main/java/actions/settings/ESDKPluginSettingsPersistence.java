package actions.settings;

import com.intellij.openapi.components.PersistentStateComponent;
import com.intellij.openapi.components.ServiceManager;
import com.intellij.openapi.components.State;
import com.intellij.openapi.components.Storage;
import com.intellij.util.xmlb.XmlSerializerUtil;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

/**
 * Saving ESDK Plugin Settings persistent.
 */
@State(
        name = "ESDKPluginSettingsPersistence",
        storages = {
                @Storage(value = "ESDKPluginSettingsPersistence.xml")}
)
public class ESDKPluginSettingsPersistence implements PersistentStateComponent<ESDKPluginSettingsPersistence> {

    private String abasGuiPath, abasGui2Path, ScreenEditorPath, defaults;

    /**
     * Instantiates a new Esdk plugin settings persistence.
     */
    public ESDKPluginSettingsPersistence() {
    }


    /**
     * Gets instance.
     *
     * @return the instance
     */
    @Nullable
    public static ESDKPluginSettingsPersistence getInstance() {
        return ServiceManager.getService(ESDKPluginSettingsPersistence.class);
    }

    @Nullable
    @Override
    public ESDKPluginSettingsPersistence getState() {
        return this;
    }

    @Override
    public void loadState(@NotNull final ESDKPluginSettingsPersistence esdkPluginSettingsPersistence) {
        XmlSerializerUtil.copyBean(esdkPluginSettingsPersistence, this);
    }

    /**
     * Gets abas gui path.
     *
     * @return the abas gui path
     */
    public String getAbasGuiPath() {
        return abasGuiPath;
    }

    /**
     * Sets abas gui path.
     *
     * @param abasGuiPath the abas gui path
     */
    public void setAbasGuiPath(final String abasGuiPath) {
        this.abasGuiPath = abasGuiPath;
    }

    /**
     * Gets screen editor path.
     *
     * @return the screen editor path
     */
    public String getScreenEditorPath() {
        return ScreenEditorPath;
    }

    /**
     * Sets screen editor path.
     *
     * @param screenEditorPath the screen editor path
     */
    public void setScreenEditorPath(final String screenEditorPath) {
        ScreenEditorPath = screenEditorPath;
    }

    /**
     * Gets abas gui 2 path.
     *
     * @return the abas gui 2 path
     */
    public String getAbasGui2Path() {
        return abasGui2Path;
    }

    /**
     * Sets abas gui 2 path.
     *
     * @param abasGui2Path the abas gui 2 path
     */
    public void setAbasGui2Path(final String abasGui2Path) {
        this.abasGui2Path = abasGui2Path;
    }

    /**
     * Gets defaults.
     *
     * @return the defaults
     */
    public String getDefaults() {
        return defaults;
    }

    /**
     * Sets defaults.
     *
     * @param defaults the defaults
     */
    public void setDefaults(final String defaults) {
        this.defaults = defaults;
    }
}
