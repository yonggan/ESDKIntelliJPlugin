package actions.settings;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import utils.Notifications;

import javax.swing.*;
import javax.swing.filechooser.FileNameExtensionFilter;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;
import java.nio.file.Paths;

/**
 * This creates the ESDK Plugin Settings in IntelliJ Settings.
 */
class PluginSettings extends ESDKPluginSettings {
    private JPanel rootPanel;
    private JTabbedPane tabbedPane1;
    private JTextField abasGuiPath;
    private JTextField screenEditorPath;
    @Nullable
    private JTextArea projectDefaults;
    private JButton resetButton;
    private JButton browseAbasGuiPath;
    private JButton browseScreenEditorPath;
    private JTextField abasGui2Path;
    private JButton browseAbasGui2Path;

    /**
     * Gets root panel.
     *
     * @return the root panel
     */
    public JPanel getRootPanel() {
        return rootPanel;
    }

    /**
     * Sets root panel.
     *
     * @param rootPanel the root panel
     */
    public void setRootPanel(final JPanel rootPanel) {
        this.rootPanel = rootPanel;
    }

    /**
     * Gets tabbed pane 1.
     *
     * @return the tabbed pane 1
     */
    public JTabbedPane getTabbedPane1() {
        return tabbedPane1;
    }

    /**
     * Sets tabbed pane 1.
     *
     * @param tabbedPane1 the tabbed pane 1
     */
    public void setTabbedPane1(final JTabbedPane tabbedPane1) {
        this.tabbedPane1 = tabbedPane1;
    }

    /**
     * Gets abas gui path.
     *
     * @return the abas gui path
     */
    public JTextField getAbasGuiPath() {
        return abasGuiPath;
    }

    /**
     * Sets abas gui path.
     *
     * @param abasGuiPath the abas gui path
     */
    public void setAbasGuiPath(final JTextField abasGuiPath) {
        this.abasGuiPath = abasGuiPath;
    }

    /**
     * Gets screen editor path.
     *
     * @return the screen editor path
     */
    public JTextField getScreenEditorPath() {
        return screenEditorPath;
    }

    /**
     * Sets screen editor path.
     *
     * @param screenEditorPath the screen editor path
     */
    public void setScreenEditorPath(final JTextField screenEditorPath) {
        this.screenEditorPath = screenEditorPath;
    }

    /**
     * Gets project defaults.
     *
     * @return the project defaults
     */
    @Nullable
    public JTextArea getProjectDefaults() {
        return projectDefaults;
    }

    /**
     * Sets project defaults.
     *
     * @param projectDefaults the project defaults
     */
    public void setProjectDefaults(final JTextArea projectDefaults) {
        this.projectDefaults = projectDefaults;
    }

    /**
     * Gets abas gui 2 path.
     *
     * @return the abas gui 2 path
     */
    public JTextField getAbasGui2Path() {
        return abasGui2Path;
    }

    /**
     * Sets abas gui 2 path.
     *
     * @param abasGui2Path the abas gui 2 path
     */
    public void setAbasGui2Path(final JTextField abasGui2Path) {
        this.abasGui2Path = abasGui2Path;
    }

    private void createUIComponents() {
        abasGuiPath = new JTextField(ESDKPluginSettingsPersistence.getInstance().getAbasGuiPath());
        abasGui2Path = new JTextField(ESDKPluginSettingsPersistence.getInstance().getAbasGui2Path());
        screenEditorPath = new JTextField(ESDKPluginSettingsPersistence.getInstance().getScreenEditorPath());

        if (ESDKPluginSettingsPersistence.getInstance().getDefaults() != null &&
                ESDKPluginSettingsPersistence.getInstance().getDefaults().isEmpty()) {
            projectDefaults = new JTextArea(loadDefaults());
        } else {
            projectDefaults = new JTextArea(ESDKPluginSettingsPersistence.getInstance().getDefaults());
        }


        resetButton = new JButton();
        resetButton.addActionListener(e -> {
            final String defaults = loadDefaults();
            getProjectDefaults().setText(defaults);
            ESDKPluginSettingsPersistence.getInstance().setDefaults(defaults);
        });

        browseAbasGuiPath = new JButton();
        browseAbasGui2Path = new JButton();
        browseScreenEditorPath = new JButton();

        browseAbasGuiPath.addActionListener(e -> {
            abasGuiPath.setText(browseFile());
        });

        browseAbasGui2Path.addActionListener(e -> {
            abasGui2Path.setText(browseFile());
        });

        browseScreenEditorPath.addActionListener(e -> {
            screenEditorPath.setText(browseFile());
        });
    }

    /**
     * Load defaults string.
     *
     * @return default json config as string
     */
    @Nullable
    public String loadDefaults() {
        try {
            final InputStream is = getClass().getResourceAsStream("/defaults/defaultValues.json");
            final InputStreamReader isr = new InputStreamReader(is, StandardCharsets.UTF_8);
            final BufferedReader br = new BufferedReader(isr);
            final StringBuilder sb = new StringBuilder();
            String line;
            while ((line = br.readLine()) != null) {
                sb.append(line + "\n");
            }
            br.close();
            isr.close();
            is.close();
            return sb.toString();

/*
Can´t be used because Plugin is packed as JAR and the File has to be read as Stream
return gson.fromJson(new FileReader(new File(getClass().getResource("/defaults/defaultValues.json").getFile())), JsonObject.class);
*/
        } catch (@NotNull final IOException e) {
            Notifications.errorNotification("Can´t find default Values!");
        }
        return null;
    }

    private String browseFile() {
        final JFileChooser jFileChooser = new JFileChooser();
        jFileChooser.addChoosableFileFilter(new FileNameExtensionFilter("Executables", "exe"));
        jFileChooser.setAcceptAllFileFilterUsed(true);
        jFileChooser.showOpenDialog(new JPanel());
        if (jFileChooser.getSelectedFile() != null) {
            return (Paths.get(jFileChooser.getSelectedFile().getAbsolutePath()).toString());
        }
        return "";
    }
}
