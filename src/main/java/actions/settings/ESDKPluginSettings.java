package actions.settings;

import com.intellij.openapi.options.SearchableConfigurable;
import org.jetbrains.annotations.Nls;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import javax.swing.*;

/**
 * ESDK Plugin Settings in IntelliJ Settings.
 */
class ESDKPluginSettings implements SearchableConfigurable {

    private PluginSettings pluginSettings;

    @NotNull
    @Override
    public String getId() {
        return "preference.ESDKPluginSettings";
    }

    @NotNull
    @Nls(capitalization = Nls.Capitalization.Title)
    @Override
    public String getDisplayName() {
        return "ESDK-Plugin Settings";
    }

    @Nullable
    @Override
    public JComponent createComponent() {
        pluginSettings = new PluginSettings();
        return pluginSettings.getRootPanel();
    }

    @Override
    public boolean isModified() {
        return true;
    }

    @Override
    public void apply() {
        if (!pluginSettings.getAbasGuiPath().getText().isEmpty()) {
            ESDKPluginSettingsPersistence.getInstance().setAbasGuiPath(pluginSettings.getAbasGuiPath().getText());
        }

        if (!pluginSettings.getAbasGui2Path().getText().isEmpty()) {
            ESDKPluginSettingsPersistence.getInstance().setAbasGui2Path(pluginSettings.getAbasGui2Path().getText());
        }

        if (!pluginSettings.getScreenEditorPath().getText().isEmpty()) {
            ESDKPluginSettingsPersistence.getInstance().setScreenEditorPath(pluginSettings.getScreenEditorPath().getText());
        }

        if (!pluginSettings.getProjectDefaults().getText().isEmpty()) {
            ESDKPluginSettingsPersistence.getInstance().setDefaults(pluginSettings.getProjectDefaults().getText());
        }
    }
}
