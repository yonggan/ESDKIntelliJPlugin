package icons;

import com.intellij.openapi.util.IconLoader;

import javax.swing.*;

/**
 * This interface contains all Icons
 */
public interface Icons {


    /**
     * The constant esdkBoat.
     */
    Icon ESDK_BOAT = IconLoader.getIcon("/images/esdkboat.svg");

    /**
     * The constant esdkBoatTool.
     */
    Icon ESDK_BOAT_RED = IconLoader.getIcon("/images/esdkboat_red.svg");

    /**
     * The constant ESDK_BOAT_GUTTER.
     */
    Icon ESDK_BOAT_GREEN = IconLoader.getIcon("/images/esdkboat_green.svg");

    /**
     * The constant ESDK_BOAT_ABAS_GUI.
     */
    Icon ESDK_BOAT_ABAS_GUI = IconLoader.getIcon("/images/actions/esdkboatabasgui.svg");
    /**
     * The constant ESDK_BOAT_EVENT.
     */
    Icon ESDK_BOAT_EVENT = IconLoader.getIcon("/images/actions/esdkboatevent.svg");
    /**
     * The constant ESDK_BOAT_EVENT_NOT_SPECIFIC.
     */
    Icon ESDK_BOAT_EVENT_NOT_SPECIFIC = IconLoader.getIcon("/images/actions/esdkboateventnotspecific.svg");
    /**
     * The constant ESDK_BOAT_EVENT_GENERATE.
     */
    Icon ESDK_BOAT_EVENT_GENERATE = IconLoader.getIcon("/images/actions/esdkboatgenerate.svg");
    /**
     * The constant ESDK_BOAT_EVENT_REGISTER.
     */
    Icon ESDK_BOAT_EVENT_REGISTER = IconLoader.getIcon("/images/actions/esdkboatregister.svg");
    /**
     * The constant ESDK_BOAT_EVENT_SCREEN_EDITOR.
     */
    Icon ESDK_BOAT_EVENT_SCREEN_EDITOR = IconLoader.getIcon("/images/actions/esdkboatscreeneditor.svg");
    /**
     * The constant ESDK_BOAT_EVENT_SETTINGS.
     */
    Icon ESDK_BOAT_EVENT_SETTINGS = IconLoader.getIcon("/images/actions/esdkboatsettings.svg");
}
