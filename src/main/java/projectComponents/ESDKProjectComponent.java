package projectComponents;

import actions.properties.PropertiesWidget;
import com.intellij.openapi.application.ApplicationManager;
import com.intellij.openapi.components.ProjectComponent;
import com.intellij.openapi.project.Project;
import com.intellij.openapi.project.ProjectManager;
import com.intellij.openapi.wm.StatusBar;
import com.intellij.openapi.wm.WindowManager;

import java.util.ArrayList;
import java.util.List;

/**
 * The type Esdk project component.
 */
public class ESDKProjectComponent implements ProjectComponent {

    private static List<PropertiesWidget> propertiesWidgets = new ArrayList<>();

    /**
     * Add status bar widget.
     */
    public static void addStatusBarWidget() {
        final Project[] projects = ProjectManager.getInstance().getOpenProjects();
        for (final Project project : projects) {
            ApplicationManager.getApplication().invokeLater(() -> {
                if (!project.isDisposed()) {
                    final StatusBar statusBar = WindowManager.getInstance().getStatusBar(project);
                    if (statusBar != null && statusBar.getWidget("ESDKGradlePropertiesWidget") == null) {
                        final PropertiesWidget propertiesWidget = new PropertiesWidget(project);
                        propertiesWidgets.add(propertiesWidget);
                        statusBar.addWidget(propertiesWidget);
                    }
                }
            });
        }
    }

    @Override
    public void projectOpened() {
        addStatusBarWidget();
    }
}
