package code;

import com.intellij.codeInsight.daemon.RelatedItemLineMarkerInfo;
import com.intellij.codeInsight.daemon.RelatedItemLineMarkerProvider;
import com.intellij.codeInsight.navigation.NavigationGutterIconBuilder;
import com.intellij.psi.PsiAnnotation;
import com.intellij.psi.PsiElement;
import com.intellij.psi.impl.source.PsiClassImpl;
import icons.Icons;
import module.builder.ProjectReader;
import org.jetbrains.annotations.NotNull;

import java.util.Collection;

/**
 * IntelliJ Line Marker Provider
 */
public class LineMarkerProvider extends RelatedItemLineMarkerProvider implements Icons {
    @Override
    protected void collectNavigationMarkers(@NotNull final PsiElement element, @NotNull final Collection<? super RelatedItemLineMarkerInfo> result) throws ArrayIndexOutOfBoundsException, NullPointerException {
        if (element instanceof PsiAnnotation) {
            final PsiAnnotation annotation = (PsiAnnotation) element;
            final String value = annotation.getQualifiedName() instanceof String ? annotation.getQualifiedName() : null;
            if (value != null) {
                if (value.startsWith("ButtonEventHandler") || value.startsWith("FieldEventHandler") || value.startsWith("ScreenEventHandler")
                        || value.startsWith("RowEventHandler")) {
                    final NavigationGutterIconBuilder<PsiElement> builder = NavigationGutterIconBuilder.create(ESDK_BOAT)
                            .setTarget(annotation.getParameterList().getAttributes()[0].getValue().getChildren()[0])
                            .setTooltipText("Navigate to field");
                    result.add(builder.createLineMarkerInfo(element));
                } else if (value.startsWith("RunFopWith")) {
                    final NavigationGutterIconBuilder<PsiElement> builder = NavigationGutterIconBuilder.create(ESDK_BOAT)
                            .setTarget(annotation.getParameterList().getAttributes()[0].getValue().getChildren()[0])
                            .setTooltipText("Navigate to Class");
                    result.add(builder.createLineMarkerInfo(element));
                } else if (value.startsWith("RunFop")) {
                    final NavigationGutterIconBuilder<PsiElement> builder = NavigationGutterIconBuilder.create(ESDK_BOAT)
                            .setTarget(element)
                            .setTooltipText("Navigate to Class");
                    result.add(builder.createLineMarkerInfo(element));
                }
            }
        } else if (element instanceof PsiClassImpl) {
            final PsiClassImpl psiClass = (PsiClassImpl) element;
            if (psiClass.getQualifiedName() == null) return;
            if (psiClass.getAnnotation("de.abas.erp.axi2.annotation.EventHandler") == null) return;
            final NavigationGutterIconBuilder<PsiElement> builder;
            if (ProjectReader.isfopJsonContainingClass(element.getProject().getBasePath(), psiClass.getQualifiedName()))
                builder = NavigationGutterIconBuilder.create(ESDK_BOAT_GREEN)
                        .setTarget(psiClass.getNameIdentifier())
                        .setTooltipText("Navigate to Class");
            else
                builder = NavigationGutterIconBuilder.create(ESDK_BOAT_RED)
                        .setTarget(psiClass.getNameIdentifier())
                        .setTooltipText("Navigate to Class");
            result.add(builder.createLineMarkerInfo(psiClass.getNameIdentifier()));
        }
    }
}