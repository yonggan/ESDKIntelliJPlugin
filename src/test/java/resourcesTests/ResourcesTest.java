package resourcesTests;

import org.hamcrest.CoreMatchers;
import org.jetbrains.annotations.NotNull;
import org.junit.Test;
import utils.FileUtils;

import java.io.*;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

import static org.hamcrest.CoreMatchers.not;
import static org.junit.Assert.*;

/**
 * The type Resources test.
 */
public class ResourcesTest {

    /**
     * Unzip.
     *
     * @param zipFilePath   the zip file path
     * @param unzipLocation the unzip location
     * @throws IOException the io exception
     */
    public static void unzip(@NotNull final String zipFilePath, @NotNull final String unzipLocation) throws IOException {

        if (!(Files.exists(Paths.get(unzipLocation)))) {
            Files.createDirectories(Paths.get(unzipLocation));
        }
        try (final ZipInputStream zipInputStream = new ZipInputStream(new FileInputStream(zipFilePath))) {
            ZipEntry entry = zipInputStream.getNextEntry();
            while (entry != null) {
                final Path filePath = Paths.get(unzipLocation, entry.getName());
                if (!entry.isDirectory()) {
                    unzipFiles(zipInputStream, filePath);
                } else {
                    Files.createDirectories(filePath);
                }

                zipInputStream.closeEntry();
                entry = zipInputStream.getNextEntry();
            }
        }
    }

    private static void unzipFiles(@NotNull final ZipInputStream zipInputStream, @NotNull final Path unzipFilePath) throws IOException {
        try (final BufferedOutputStream bos = new BufferedOutputStream(
                new FileOutputStream(unzipFilePath.toAbsolutePath().toString()))) {
            final byte[] bytesIn = new byte[1024];
            int read;
            while ((read = zipInputStream.read(bytesIn)) != -1) {
                bos.write(bytesIn, 0, read);
            }
        }
    }

    private static void copyFileUsingStream(final InputStream source, @NotNull final File dest) throws IOException {
        try (final InputStream is = source; final OutputStream os = new FileOutputStream(dest)) {
            final byte[] buffer = new byte[1024];
            int length;
            while ((length = is.read(buffer)) > 0) {
                os.write(buffer, 0, length);
            }
        }
    }

    private static void deleteFile(@NotNull final String filePath) {
        new File(filePath).delete();
        new File(filePath.replaceAll("/", "\\\\")).delete();
    }

    /**
     * Color scheme.
     */
    @Test
    public void colorScheme() {
        assertNotNull(FileUtils.class.getResourceAsStream("/colorScheme/Infinity.xml"));
    }

    /**
     * Defaults.
     */
    @Test
    public void defaults() {
        assertNotNull(FileUtils.class.getResourceAsStream("/defaults/defaultValues.json"));
    }

    /**
     * File templates.
     */
    @Test
    public void fileTemplates() {
//######################################################################################################################
//#         Code Templates                                                                                             #
//######################################################################################################################
        assertNotNull(FileUtils.class.getResourceAsStream("/fileTemplates/codeTemplates/button/buttonEvHa.vm"));

        assertNotNull(FileUtils.class.getResourceAsStream("/fileTemplates/codeTemplates/dbselection/headandtableSel.vm"));
        assertNotNull(FileUtils.class.getResourceAsStream("/fileTemplates/codeTemplates/dbselection/headSel.vm"));
        assertNotNull(FileUtils.class.getResourceAsStream("/fileTemplates/codeTemplates/dbselection/tableSel.vm"));

        assertNotNull(FileUtils.class.getResourceAsStream("/fileTemplates/codeTemplates/field/fieldEvHa.vm"));

        assertNotNull(FileUtils.class.getResourceAsStream("/fileTemplates/codeTemplates/row/rowEvHa.vm"));

        assertNotNull(FileUtils.class.getResourceAsStream("/fileTemplates/codeTemplates/screen/screenEvHa.vm"));

        assertNotNull(FileUtils.class.getResourceAsStream("/fileTemplates/codeTemplates/standalone/standalone.vm"));

        assertNotNull(FileUtils.class.getResourceAsStream("/fileTemplates/codeTemplates/tools/globalTextBuffer.vm"));
        assertNotNull(FileUtils.class.getResourceAsStream("/fileTemplates/codeTemplates/tools/visibilityAppend.vm"));
        assertNotNull(FileUtils.class.getResourceAsStream("/fileTemplates/codeTemplates/tools/visibilityDelete.vm"));
        assertNotNull(FileUtils.class.getResourceAsStream("/fileTemplates/codeTemplates/tools/visibilityReset.vm"));

//######################################################################################################################
//#         Infosystem Templates                                                                                       #
//######################################################################################################################

        assertNotNull(FileUtils.class.getResourceAsStream("/fileTemplates/infosystem/infosystemEvHa.vm"));

//######################################################################################################################
//#         Mask Templates                                                                                             #
//######################################################################################################################

        assertNotNull(FileUtils.class.getResourceAsStream("/fileTemplates/mask/maskEvHa.vm"));

//######################################################################################################################
//#         Standalone Templates                                                                                       #
//######################################################################################################################

        assertNotNull(FileUtils.class.getResourceAsStream("/fileTemplates/standalone/standalone.vm"));
    }

    /**
     * Idea templates.
     */
    @Test
    public void ideaTemplates() {
        assertNotNull(FileUtils.class.getResourceAsStream("/ideaTemplates/gradle.xml"));
    }

    /**
     * Images.
     */
    @Test
    public void images() {
        assertNotNull(FileUtils.class.getResourceAsStream("/images/esdkboat.svg"));
        assertNotNull(FileUtils.class.getResourceAsStream("/images/esdkboat_dark.svg"));
        assertNotNull(FileUtils.class.getResourceAsStream("/images/esdkboat_green.svg"));
        assertNotNull(FileUtils.class.getResourceAsStream("/images/esdkboat_red.svg"));

        assertNotNull(FileUtils.class.getResourceAsStream("/images/actions/esdkboatabasgui.svg"));
        assertNotNull(FileUtils.class.getResourceAsStream("/images/actions/esdkboatevent.svg"));
        assertNotNull(FileUtils.class.getResourceAsStream("/images/actions/esdkboateventnotspecific.svg"));
        assertNotNull(FileUtils.class.getResourceAsStream("/images/actions/esdkboatgenerate.svg"));
        assertNotNull(FileUtils.class.getResourceAsStream("/images/actions/esdkboatregister.svg"));
        assertNotNull(FileUtils.class.getResourceAsStream("/images/actions/esdkboatscreeneditor.svg"));
        assertNotNull(FileUtils.class.getResourceAsStream("/images/actions/esdkboatsettings.svg"));
    }

    /**
     * Live templates.
     *
     * @throws IOException the io exception
     */
    @Test
    public void liveTemplates() throws IOException {
        assertNotNull(FileUtils.class.getResourceAsStream("/liveTemplates/AbasESDK.xml"));
        final InputStream is = getClass().getResourceAsStream("/liveTemplates/AbasESDK.xml");
        final InputStreamReader isr = new InputStreamReader(is, StandardCharsets.UTF_8);
        final BufferedReader br = new BufferedReader(isr);
        final StringBuilder sb = new StringBuilder();
        String line;
        while ((line = br.readLine()) != null) {
            sb.append(line);
        }
        br.close();
        isr.close();
        is.close();
        assertThat(sb.toString(), not(CoreMatchers.containsString("<context/>")));
    }

    /**
     * Template.
     *
     * @throws IOException the io exception
     */
    @Test
    public void template() throws IOException {
        final String ARCHIVE_EXTENSION = ".zip";
        final String TEMPLATE_NAME = "/mctrigger";
        assertNotNull(FileUtils.class.getResourceAsStream("/template" + TEMPLATE_NAME + ARCHIVE_EXTENSION));
        final Path tmp = Files.createTempDirectory("tmp");
        createProject(tmp, TEMPLATE_NAME, ARCHIVE_EXTENSION);
        assertTrue(Files.exists(Paths.get(tmp.toString() + "/build.gradle")));
        assertTrue(Files.exists(Paths.get(tmp.toString() + "/docker-compose.yml")));
        assertTrue(Files.exists(Paths.get(tmp.toString() + "/gradle.properties")));
    }

    private void createProject(@NotNull final Path tmp, final String TEMPLATE_NAME, final String ARCHIVE_EXTENSION) throws IOException {


        final InputStream file = getClass().getResourceAsStream("/template" + TEMPLATE_NAME + ARCHIVE_EXTENSION);
        FileUtils.copyFileUsingStream(
                file, new File(tmp.toString() + TEMPLATE_NAME + ARCHIVE_EXTENSION));

        FileUtils.unzip(
                tmp.toString() + TEMPLATE_NAME + ARCHIVE_EXTENSION,
                tmp.toString());
        FileUtils.deleteFile(tmp.toString() + TEMPLATE_NAME + ARCHIVE_EXTENSION);
    }

}
